/*ckwg +29
 * Copyright 2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "VideoEncoder.h"

#include "GuiCommon.h"

#include <krest/util/unique.hpp>

#include <arrows/qt/image_container.h>

#include <vital/algo/video_output.h>

#include <qtStlUtil.h>

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QThread>

namespace kv = kwiver::vital;
namespace kaq = kwiver::arrows::qt;

// ============================================================================
class VideoEncoderPrivate
{
public:
  bool closed = false;
  bool tried_open = false;

  QString path;
  QThread thread;
  kv::video_settings_sptr settings;
  kv::algo::video_output_sptr out;
};

QTE_IMPLEMENT_D_FUNC(VideoEncoder)

// ----------------------------------------------------------------------------
VideoEncoder::VideoEncoder(QObject* parent)
  : QObject{parent}, d_ptr{new VideoEncoderPrivate}
{
  QTE_D();

  connect(&d->thread, &QThread::finished, this, &VideoEncoder::finished);
}

// ----------------------------------------------------------------------------
VideoEncoder::~VideoEncoder()
{
  QTE_D();

  if (d->thread.isRunning())
  {
    this->close();
  }

  this->join();
}

// ----------------------------------------------------------------------------
bool VideoEncoder::start(QString const& outputPath, kv::video_settings_sptr settings)
{
  QTE_D();

  if (d->out)
  {
    qWarning() << "VideoEncoder::start:"
               << "cannot start a process that was already started";
    return false;
  }

  // Move to worker thread
  d->thread.start();
  this->moveToThread(&d->thread);

  d->path = outputPath;
  d->settings = settings;

  return true;
}

// ----------------------------------------------------------------------------
void VideoEncoder::join()
{
  QTE_D();

  this->close();
  d->thread.wait();
}

// ----------------------------------------------------------------------------
void VideoEncoder::writeFrame(kv::image_container_sptr image)
{
  if (!image)
  {
    return;
  }

  QTE_THREAD_INVOKE(this->writeFrame, image);

  QTE_D();

  if (!d->out && !d->tried_open)
  {
    // Open output file
    d->tried_open = true;
    auto const exe_path = QApplication::applicationFilePath();
    auto const prefix = QFileInfo(exe_path).dir().absoluteFilePath("..");
    auto const file_config = readConfig("gui_video_writer.conf", prefix);
    if( file_config )
    {
      kv::algo::video_output::set_nested_algo_configuration(
        "video_output", file_config, d->out );
      auto config = d->out->get_configuration();
      config->set_value( "width", image->width() );
      config->set_value( "height", image->height() );
      d->out->set_configuration( config );
      d->out->open(stdString(d->path), d->settings.get());
    }
  }

  if (d->out)
  {
    d->out->add_image(image, {});
  }
}

// ----------------------------------------------------------------------------
void VideoEncoder::close()
{
  QTE_THREAD_INVOKE(this->close);

  QTE_D();

  if (d->out)
  {
    d->out->close();
    d->out.reset();
    d->closed = true;
    d->thread.quit();
  }
}

// ----------------------------------------------------------------------------
void VideoEncoder::cancel()
{
  this->close();
}
