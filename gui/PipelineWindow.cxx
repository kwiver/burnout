/*ckwg +29
 * Copyright 2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "PipelineWindow.h"
#include "ui_PipelineWindow.h"

#include "PipelineWorker.h"
#include "VideoEncoder.h"

#include <QPointer>

using krest::core::VideoMetaData;

using kwiver::vital::config_block_sptr;
using kwiver::vital::image_container_sptr;

// ============================================================================
class PipelineWindowPrivate
{
public:
  PipelineWindowPrivate(QWidget* q) : worker{q} {}

  Ui::PipelineWindow ui;

  PipelineWorker worker;
  bool hasVideo = false;
  bool hasPipeline = false;

  image_container_sptr lastImage;
};

QTE_IMPLEMENT_D_FUNC(PipelineWindow)

// ----------------------------------------------------------------------------
PipelineWindow::PipelineWindow(QWidget* parent, Qt::WindowFlags flags)
  : QDialog{parent, flags}, d_ptr{new PipelineWindowPrivate{this}}
{
  QTE_D();

  qRegisterMetaType<image_container_sptr>();

  d->ui.setupUi(this);

  connect(&d->worker, &PipelineWorker::progressRangeChanged, this,
          [d](int min, int max){ d->ui.progress->setRange(min, max); });
  connect(&d->worker, &PipelineWorker::progressValueChanged, this,
          [d](int value){ d->ui.progress->setValue(value); });

  connect(
    &d->worker, &PipelineWorker::inpaintedImageAvailable, this,
    [d](image_container_sptr const& image)
    {
      if (!d->ui.actionPause->isChecked() || !d->lastImage)
      {
        d->ui.view->setImage(image, VideoMetaData{});
      }
      if (!d->lastImage)
      {
        auto const iw = static_cast<qreal>(image->width());
        auto const ih = static_cast<qreal>(image->height());
        auto const viewSize = QSizeF{d->ui.view->size()};
        auto const zw = viewSize.width() / iw;
        auto const zh = viewSize.height() / ih;
        d->ui.view->setZoom(static_cast<float>(qMin(zw, zh)));
      }
      d->lastImage = image;
    });

  connect(
    d->ui.actionPause, &QAction::toggled, this,
    [d](bool checked)
    {
      if (!checked && d->lastImage)
      {
        d->ui.view->setImage(d->lastImage, VideoMetaData{});
      }
    });

  connect(
    &d->worker, &PipelineWorker::endOfInput, this,
    [d]
    {
      d->ui.actionCancel->setEnabled(false);
      d->ui.progress->setRange(0, 0);
    });
  connect(&d->worker, &PipelineWorker::finished,
          d->ui.progress, &QWidget::hide);

  connect(d->ui.actionCancel, &QAction::triggered,
          this, &PipelineWindow::cancel);
}

// ----------------------------------------------------------------------------
PipelineWindow::~PipelineWindow()
{
}

// ----------------------------------------------------------------------------
void PipelineWindow::setVideo(
  config_block_sptr const& config, std::string const& path)
{
  QTE_D();

  d->worker.setVideo(config, path);
  d->hasVideo = true;
}

// ----------------------------------------------------------------------------
bool PipelineWindow::setPipeline(QString const& pipelineFile)
{
  QTE_D();
  return (d->hasPipeline = d->worker.initialize(pipelineFile));
}

// ----------------------------------------------------------------------------
void PipelineWindow::attachEncoder(
  std::unique_ptr<VideoEncoder>&& encoder, ImageSource source)
{
  QTE_D();

  auto* const e = encoder.release();

  switch (source)
  {
    case ImageSource::InpaintedImage:
      connect(&d->worker, &PipelineWorker::inpaintedImageAvailable,
              e, &VideoEncoder::writeFrame);
      break;
    case ImageSource::MaskImage:
      connect(&d->worker, &PipelineWorker::maskImageAvailable,
              e, &VideoEncoder::writeFrame);
      break;
  }

  connect(&d->worker, &PipelineWorker::finished, e, &VideoEncoder::close);
  connect(e, &VideoEncoder::finished, this, [e]{ delete e; },
          Qt::QueuedConnection);
}

// ----------------------------------------------------------------------------
int PipelineWindow::exec()
{
  QTE_D();

  if (!d->hasVideo || !d->hasPipeline)
  {
    return QDialog::Rejected;
  }

  this->show();

  d->worker.execute();

  return QDialog::exec();
}

// ----------------------------------------------------------------------------
void PipelineWindow::cancel()
{
  QTE_D();

  d->ui.actionCancel->setEnabled(false);
  d->worker.cancel();
}
