/*ckwg +29
 * Copyright 2016-2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MainWindow.h"
#include "am_MainWindow.h"
#include "ui_MainWindow.h"

#include "AboutDialog.h"
#include "BurnoutDialog.h"
#include "GuiCommon.h"
#include "PipelineWindow.h"
#include "VideoEncoder.h"
#include "VideoImport.h"

#include "version.h"

#include <krest/util/unique.hpp>

#include <kwiversys/SystemTools.hxx>

#include <arrows/klv/klv_metadata.h>

#include <vital/algo/metadata_map_io.h>
#include <vital/algo/video_input.h>
#include <vital/config/config_block.h>
#include <vital/exceptions.h>
#include <vital/types/metadata_map.h>

#include <qtEnumerate.h>
#include <qtGet.h>
#include <qtIndexRange.h>
#include <qtMath.h>
#include <qtStlUtil.h>
#include <qtUiState.h>
#include <qtUiStateItem.h>

#include <QApplication>
#include <QColorDialog>
#include <QDebug>
#include <QDesktopServices>
#include <QDropEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QMimeData>
#include <QTimer>
#include <QUrl>

#include <functional>
#include <fstream>
#include <iomanip>

namespace kv = kwiver::vital;
namespace klv = kwiver::arrows::klv;

///////////////////////////////////////////////////////////////////////////////

//BEGIN miscellaneous helpers

namespace // anonymous
{

//-----------------------------------------------------------------------------
QString findUserManual(QString const& prefix)
{
  static auto const name = "burnout.html";
  static auto const product = "burnout";
  static auto const version = BURNOUT_VERSION;

  auto locations = QStringList();

  // Install location
  locations.append(QString("%1/share/doc/%2-%3").arg(prefix, product, version));

  // Build location
  locations.append(QString("%1/doc").arg(prefix));

  foreach (auto const& path, locations)
  {
    auto const fi = QFileInfo(QString("%1/user/%2").arg(path, name));
    if (fi.exists())
    {
      // Found manual
      return fi.canonicalFilePath();
    }
  }

  // Manual not found
  return QString();
}

//-----------------------------------------------------------------------------
QSet<QString> supportedImageExtensions()
{
  auto result = QSet<QString>{
    QStringLiteral("jpg"),
    QStringLiteral("jpeg"),
    QStringLiteral("png"),
  };

  return result;
}

//-----------------------------------------------------------------------------
QSet<QString> supportedVideoExtensions()
{
  QSet<QString> result;

  // For now just add some common extensions we expect to encounter
  result.insert("mpeg");
  result.insert("mpg");
  result.insert("mp4");
  result.insert("avi");
  result.insert("wmw");
  result.insert("mov");
  result.insert("ts");
  result.insert("txt"); // image list

  return result;
}

//-----------------------------------------------------------------------------
QString imageConfigForPath(QString const& path, QString const& type)
{
  static const auto configTemplate = QString{"gui_%1_%2_reader.conf"};

  const auto ext = QFileInfo{path}.suffix();
  const QString mode = (ext.toLower() == "txt" ? "list" : "video");
  return configTemplate.arg(type, mode);
}

//-----------------------------------------------------------------------------
QString makeFilters(QStringList const& extensions)
{
  auto result = QStringList();
  foreach (auto const& extension, extensions)
  {
    result.append("*." + extension);
  }
  return result.join(" ");
}

//-----------------------------------------------------------------------------
template <typename T>
class StateValue : public qtUiState::AbstractItem
{
public:
  StateValue(T const& defaultValue = T{})
    : data{defaultValue} {}

  operator T() const { return this->data; }

  StateValue& operator=(T const& newValue)
  {
    this->data = newValue;
    return *this;
  }

  QVariant value() const override
  {
    return QVariant::fromValue(this->data);
  }

  void setValue(QVariant const& newValue) override
  {
    this->data = newValue.value<T>();
  }

protected:
  T data;
};

} // namespace

//END miscellaneous helpers

///////////////////////////////////////////////////////////////////////////////

//BEGIN MainWindowPrivate

//-----------------------------------------------------------------------------
class MainWindowPrivate
{
public:
  // Methods
  MainWindowPrivate(MainWindow* mainWindow);

  void addVideoSource(kv::config_block_sptr const& config,
                      QString const& videoPath);
  void addMaskSource(kv::config_block_sptr const& config,
                     QString const& maskPath);

  void updateFrames(std::shared_ptr<kv::metadata_map::map_metadata_t>);


  void setActiveFrame(int);
  void updateFrameView();

  void resetView();

  std::string getFrameName(kv::frame_id_t frame);

  void loadImage(kv::frame_id_t frame);
  void loadEmptyImage();

  void updateProgress(QObject* object,
                      const QString& description = QString(""),
                      int value = 0);

  void handleLogMessage(kv::kwiver_logger::log_level_t level,
                        std::string const& name,
                        std::string const& msg,
                        kv::logger_ns::location_info const& loc);

  // Member variables
  Ui::MainWindow UI;
  Am::MainWindow AM;
  qtUiState uiState;

  QString prefix;
  std::ofstream logFileStream;

  StateValue<QColor>* viewBackgroundColor = nullptr;

  QTimer slideTimer;

  kv::config_block_sptr freestandingConfig = kv::config_block::empty_config();

  QString videoPath;
  QString maskPath;
  kv::algo::video_input_sptr videoSource;
  kv::algo::video_input_sptr maskSource;
  kv::timestamp currentVideoTimestamp;
  kv::metadata_map_sptr videoMetadataMap =
    std::make_shared<kv::simple_metadata_map>();

  kv::frame_id_t frameCount = 0;

  QSize lastImageSize;

  kv::frame_id_t activeFrameIndex = -1;

  VideoImport videoImporter;

  // Progress tracking
  QHash<QObject*, int> progressIds;
};

QTE_IMPLEMENT_D_FUNC(MainWindow)

//-----------------------------------------------------------------------------
MainWindowPrivate::MainWindowPrivate(MainWindow* mainWindow)
{
  auto const exePath = QApplication::applicationFilePath();
  this->prefix = QFileInfo{exePath}.dir().absoluteFilePath("..");

  QObject::connect(&videoImporter, &VideoImport::progressChanged,
                   mainWindow, &MainWindow::updateProgress);
  QObject::connect(&videoImporter, &VideoImport::completed,
                   mainWindow, &MainWindow::updateFrames);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::addVideoSource(
  kv::config_block_sptr const& config,
  QString const& videoPath)
{
  this->videoPath = videoPath;
  this->freestandingConfig->merge_config(config);

  // Close the existing video source if it exists
  if (this->videoSource)
  {
    this->videoSource->close();
  }

  kv::algo::video_input::set_nested_algo_configuration(
    "video_reader", config, this->videoSource);

  try
  {
    if (this->videoSource)
    {
      this->videoSource->open(stdString(videoPath));
      this->UI.actionSlideshowPlay->setEnabled(true);
      this->UI.frame->setEnabled(true);
      this->UI.frameSpin->setEnabled(true);
      this->UI.player->setEnabled(true);
      this->frameCount = videoSource->num_frames();
      this->UI.frame->setRange(1, std::max<size_t>(1, frameCount));
      this->UI.frameSpin->setRange(1, std::max<size_t>(1, frameCount));
      this->setActiveFrame(1);
    }
  }
  catch (kv::vital_exception const& e)
  {
    qWarning() << e.what();
    this->videoSource->close();
    this->videoSource.reset();
  }

  this->UI.actionRemoveBurnIn->setEnabled(this->videoSource != nullptr);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::addMaskSource(
  kv::config_block_sptr const& config, QString const& path)
{
  this->maskPath = path;
  this->freestandingConfig->merge_config(config);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::updateFrames(
  std::shared_ptr<kv::metadata_map::map_metadata_t> mdMap)
{
  if (mdMap)
  {
    this->videoMetadataMap = std::make_shared<kv::simple_metadata_map>(*mdMap);
    this->UI.metadata->updateMetadata(mdMap);
  }

  // Disconnect cancel action
  QObject::disconnect(this->UI.actionCancelComputation, nullptr,
                      &this->videoImporter, nullptr);
  this->UI.actionCancelComputation->setEnabled(false);
  this->UI.actionExportMetadata->setEnabled(true);
  this->UI.actionExportKLV->setEnabled(true);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::setActiveFrame(int id)
{
  auto oldSignalState = this->UI.frame->blockSignals(true);
  this->UI.frame->setValue(id);
  this->UI.frame->blockSignals(oldSignalState);
  this->UI.frameSpin->setValue(id);

  this->activeFrameIndex = id;

  this->updateFrameView();
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::updateFrameView()
{
  if (this->activeFrameIndex < 1 || (this->activeFrameIndex > this->frameCount))
  {
    this->loadEmptyImage();
    return;
  }

  // Show frame image
  this->loadImage(this->activeFrameIndex);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::resetView()
{
  if (!this->lastImageSize.isEmpty())
  {
    auto const extents = QSizeF{this->lastImageSize};
    auto const viewSize = QSizeF{this->UI.player->size()};
    auto const zw = viewSize.width() / extents.width();
    auto const zh = viewSize.height() / extents.height();
    this->UI.player->setZoom(static_cast<float>(qMin(zw, zh)));
    this->UI.player->setCenter({0, 0});
  }
}

//-----------------------------------------------------------------------------
std::string MainWindowPrivate::getFrameName(kv::frame_id_t frameId)
{
  if (videoMetadataMap)
  {
    auto mdv = videoMetadataMap->get_vector(frameId);
    if (!mdv.empty())
    {
      return frameName(frameId, mdv);
    }
  }
  auto dummy_md = std::make_shared<kwiver::vital::metadata>();
  dummy_md->add<kwiver::vital::VITAL_META_VIDEO_URI>(
    stdString(this->videoPath));
  return frameName(frameId, dummy_md);
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::loadEmptyImage()
{
  this->UI.player->setImage(nullptr, krest::core::VideoMetaData{});
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::loadImage(kv::frame_id_t frame)
{
  if (frame != this->currentVideoTimestamp.get_frame())
  {
    // step through next frames if the requested frame is just a few ahead
    if (this->videoSource &&
        this->currentVideoTimestamp.get_frame() < frame &&
        this->currentVideoTimestamp.get_frame() + 10 > frame)
    {
      while (this->videoSource->next_frame(this->currentVideoTimestamp) &&
             this->currentVideoTimestamp.get_frame() < frame);
    }
    // otherwise seek to the requested frame
    else if (!this->videoSource ||
        !videoSource->seek_frame(this->currentVideoTimestamp, frame))
    {
      this->loadEmptyImage();
    }
  }

  // Get frame from video source
  if (this->videoSource)
  {
    // Advance video source if it hasn't been advanced
    if (!videoSource->good())
    {
      videoSource->next_frame(this->currentVideoTimestamp);
    }

    auto image = videoSource->frame_image();
    if (!image)
    {
      qWarning() << "Failed to read image for frame " << frame;
      this->loadEmptyImage();
      return;
    }

    auto md = krest::core::VideoMetaData{};
    md.setImageName(this->getFrameName(frame));

    this->UI.player->setImage(image, md);
    this->UI.metadata->updateMetadata(videoSource->frame_metadata());

    auto const new_frame = videoSource->frame_timestamp().get_frame();
    if( activeFrameIndex != new_frame )
    {
      this->setActiveFrame( new_frame );
    }
  }
  else
  {
    this->loadEmptyImage();
  }
}

//-----------------------------------------------------------------------------
void MainWindowPrivate::updateProgress(QObject* object,
                                       const QString& description,
                                       int value)
{
  QString desc = description;
  desc.replace('&', "");
  int taskId = -1;
  if (!this->progressIds.contains(object))
  {
    if (value < 100)
    {
      taskId = this->UI.progressWidget->addTask(desc, 0, 0, 0);
      this->progressIds.insert(object, taskId);
    }
    return;
  }
  else
  {
    taskId = this->progressIds.value(object);
  }

  this->UI.progressWidget->setTaskText(taskId, desc);
  this->UI.progressWidget->setProgressValue(taskId, value);
  switch (value)
  {
    case 0:
    {
      this->UI.progressWidget->setProgressRange(taskId, 0, 0);
      break;
    }
    case 100:
    {
      this->UI.progressWidget->removeTask(taskId);
      this->progressIds.remove(object);
      break;
    }
    default:
    {
      this->UI.progressWidget->setProgressRange(taskId, 0, 100);
      break;
    }
  }
}

//-----------------------------------------------------------------------------
void MainWindowPrivate
::handleLogMessage(kv::kwiver_logger::log_level_t level,
                   std::string const& name,
                   std::string const& msg,
                   kv::logger_ns::location_info const& loc)
{
  this->UI.Logger->logHandler(level, name, msg, loc);

  if (logFileStream.is_open())
  {
    constexpr const char* const date_format = "%Y-%m-%d %H:%M:%S - ";
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);
    logFileStream << "[" << std::setfill(' ') << std::setw(5)
                  << kv::kwiver_logger::get_level_string(level) << "] "
                  << std::put_time(&tm, date_format)
                  << name << ": " << msg << std::endl;
  }
}

//END MainWindowPrivate

///////////////////////////////////////////////////////////////////////////////

//BEGIN MainWindow

//-----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags)
  : QMainWindow(parent, flags), d_ptr(new MainWindowPrivate(this))
{
  QTE_D();

  // Set up UI
  d->UI.setupUi(this);
  d->AM.setupActions(d->UI, this);

  using std::placeholders::_1;
  using std::placeholders::_2;
  using std::placeholders::_3;
  using std::placeholders::_4;
  kv::kwiver_logger::callback_t cb =
    std::bind(&MainWindowPrivate::handleLogMessage, d, _1, _2, _3, _4);
  kv::kwiver_logger::set_global_callback(cb);

  d->UI.menuView->addSeparator();
  d->UI.menuView->addAction(d->UI.metadataDock->toggleViewAction());
  d->UI.menuView->addAction(d->UI.loggerDock->toggleViewAction());

  d->UI.playSlideshowButton->setDefaultAction(d->UI.actionSlideshowPlay);
  d->UI.loopSlideshowButton->setDefaultAction(d->UI.actionSlideshowLoop);

  connect(d->UI.actionQuit, &QAction::triggered,
          qApp, &QCoreApplication::quit);

  connect(d->UI.actionOpenVideo, &QAction::triggered,
          this, &MainWindow::openImagery);
  //connect(d->UI.actionImportMasks, &QAction::triggered,
  //        this, &MainWindow::openMaskImagery);

  connect(d->UI.actionExportMetadata, &QAction::triggered,
          this, &MainWindow::saveMetadata);
  connect(d->UI.actionExportKLV, &QAction::triggered,
          this, &MainWindow::saveKLV);
  connect(d->UI.actionRemoveBurnIn, &QAction::triggered,
          this, &MainWindow::removeBurnIn);

  connect(d->UI.actionSetBackgroundColor, &QAction::triggered,
          this, &MainWindow::setViewBackroundColor);

  connect(d->UI.actionAbout, &QAction::triggered,
          this, &MainWindow::showAboutDialog);
  connect(d->UI.actionShowManual, &QAction::triggered,
          this, &MainWindow::showUserManual);

  connect(&d->slideTimer, &QTimer::timeout, this, &MainWindow::nextSlide);
  connect(d->UI.actionSlideshowPlay, &QAction::toggled,
          this, &MainWindow::setSlideshowPlaying);
  connect(d->UI.slideSpeed, &QAbstractSlider::valueChanged,
          this, &MainWindow::setSlideSpeed);

  connect(d->UI.frame, &QAbstractSlider::valueChanged,
          this, &MainWindow::setActiveFrame);

  connect(d->UI.actionViewReset, &QAction::triggered, this,
          [d]{ d->resetView(); });

  connect(
    d->UI.player, &krest::gui::Player::imageSizeChanged, this,
    [d](QSize imageSize){
      if (d->lastImageSize != imageSize)
      {
        d->lastImageSize = imageSize;
        d->resetView();
      }
    });

  this->setSlideSpeed(d->UI.slideSpeed->value());

  // Set up UI persistence and restore previous state
  auto const sdItem = new qtUiState::Item<int, QSlider>(
    d->UI.slideSpeed, &QSlider::value, &QSlider::setValue);
  d->uiState.map("SlideSpeed", sdItem);

  d->viewBackgroundColor = new StateValue<QColor>{Qt::black},
  d->uiState.map("ViewBackground", d->viewBackgroundColor);

  // d->uiState.mapChecked("Antialiasing", d->UI.actionAntialiasing);

  d->uiState.mapState("Window/state", this);
  d->uiState.mapGeometry("Window/geometry", this);
  d->uiState.restore();

  // d->UI.frameView->setBackgroundColor(*d->viewBackgroundColor);

  // Set up the progress widget
  d->UI.progressWidget->setAutoHide(true);
}

//-----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
  QTE_D();
  d->uiState.save();
}

//-----------------------------------------------------------------------------
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
  static auto const videoExtensions = supportedVideoExtensions();

  bool all_videos = false;
  if (event->mimeData()->hasUrls())
  {
    for (auto url : event->mimeData()->urls())
    {
      if (!url.isLocalFile())
      {
        return;
      }
      auto const ext = QFileInfo{ url.fileName() }.suffix().toLower();
      if (!videoExtensions.contains(ext))
      {
        return;
      }
      all_videos = true;
    }
  }
  if (all_videos)
  {
    event->acceptProposedAction();
  }
}

//-----------------------------------------------------------------------------
void MainWindow::dropEvent(QDropEvent *event)
{
  for (auto url : event->mimeData()->urls())
  {
    LOG_INFO(kv::get_logger("MainWindow"), "Opening " << stdString(url.toLocalFile()));
    loadVideo(url.toLocalFile());
    event->acceptProposedAction();
    break;
  }
}

//-----------------------------------------------------------------------------
void MainWindow::openImagery()
{
  static auto const videoFilters =
    makeFilters(supportedVideoExtensions().values());

  auto const paths = QFileDialog::getOpenFileNames(
    this, "Open Imagery", QString(),
    "All Supported Files (" + videoFilters + ");;"
                                             "Video files (" +
      videoFilters + ");;"
                     "All Files (*)");

  for (auto const& path : paths)
  {
    this->loadImagery(path);
  }
}

//-----------------------------------------------------------------------------
void MainWindow::openMaskImagery()
{
  static auto const imageFilters =
    makeFilters(supportedImageExtensions().values());
  static auto const videoFilters =
    makeFilters(supportedVideoExtensions().values());

  // TODO: Add image filters back once that is supported again.
  auto const paths = QFileDialog::getOpenFileNames(
    this, "Open Mask Imagery", QString(),
    "All Supported Files (" + videoFilters + ");;"
                                             "Video files (" +
      videoFilters + ");;"
                     "All Files (*)");

  for (auto const& path : paths)
  {
    this->loadMaskImagery(path);
  }
}

//-----------------------------------------------------------------------------
void MainWindow::loadImagery(QString const& path)
{
  static auto const videoExtensions = supportedVideoExtensions();

  auto const ext = QFileInfo{path}.suffix().toLower();
  if (videoExtensions.contains(ext))
  {
    // TODO: Handle [selection of] multiple videos better
    this->loadVideo(path);
  }
  else
  {
    qWarning() << "Don't know how to read file" << path
               << "(unrecognized extension)";
  }
}

//-----------------------------------------------------------------------------
void MainWindow::loadMaskImagery(QString const& path)
{
  static auto const imageExtensions = supportedImageExtensions();
  static auto const videoExtensions = supportedVideoExtensions();

  auto const ext = QFileInfo{path}.suffix().toLower();
  if (imageExtensions.contains(ext))
  {
    this->loadMaskImage(path);
  }
  else if (videoExtensions.contains(ext))
  {
    // TODO: Handle [selection of] multiple videos better
    this->loadMaskVideo(path);
  }
  else
  {
    qWarning() << "Don't know how to read file" << path
               << "(unrecognized extension)";
  }
}

//-----------------------------------------------------------------------------
void MainWindow::loadVideo(QString const& path)
{
  QTE_D();

  auto const& configName = imageConfigForPath(path, "image");
  auto config = readConfig(configName, d->prefix);
  if (!config)
  {
    static auto const msg =
      QStringLiteral("Failed to parse the configuration file \"%1\".");
    QMessageBox::critical(
      this, QStringLiteral("Configuration error"),
      msg.arg(configName));
    return;
  }

  try
  {
    d->addVideoSource(config, path);
  }
  catch (std::exception const& e)
  {
    QMessageBox::critical(
      this, QStringLiteral("Error loading video"),
      QStringLiteral("The video could not be loaded: ") +
      QString::fromLocal8Bit(e.what()));
    return;
  }

  QObject::connect(d->UI.actionCancelComputation, &QAction::triggered,
    &d->videoImporter, &VideoImport::cancel);
  QObject::connect(d->UI.actionQuit, &QAction::triggered,
    &d->videoImporter, &VideoImport::cancel);
  d->UI.actionCancelComputation->setEnabled(true);

  d->videoImporter.setData(config, stdString(path));
  d->videoImporter.start();
}

//-----------------------------------------------------------------------------
void MainWindow::loadMaskImage(QString const&)
{
  // TODO
}

//-----------------------------------------------------------------------------
void MainWindow::loadMaskVideo(QString const& path)
{
  QTE_D();

  auto const& configName = imageConfigForPath(path, "mask");
  auto config = readConfig(configName, d->prefix);
  if (!config)
  {
    static auto const msg =
      QStringLiteral("Failed to parse the configuration file \"%1\".");
    QMessageBox::critical(
      this, QStringLiteral("Configuration error"),
      msg.arg(configName));
    return;
  }

  try
  {
    d->addMaskSource(config, path);
  }
  catch (std::exception const& e)
  {
    QMessageBox::critical(
      this, QStringLiteral("Error loading video"),
      QStringLiteral("The video could not be loaded: ") +
      QString::fromLocal8Bit(e.what()));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::saveMetadata()
{
  QTE_D();

  auto const name = QFileInfo(d->videoPath).baseName();
  auto const path = QFileDialog::getSaveFileName(
    this, "Save Metadata", name + QString(".csv"),
    "Comma-Separated Values (*.csv);;"
    "JavaScript Object Notation (*.json);;"
    "All Files (*)");

  try
  {
    if (!path.isEmpty())
    {
      this->saveMetadataFile(path);
    }
  }
  catch (...)
  {
    static auto const msg =
      QStringLiteral("An error occurred while saving the metadata to \"%1\". "
                     "The output file may not have been written correctly.");
    QMessageBox::critical(this, "Export error", msg.arg(path));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::saveMetadataFile(QString const& path)
{
  QTE_D();

  typedef kwiversys::SystemTools ST;
  std::string writer_type;

  if (ST::GetFilenameLastExtension(stdString(path)) == ".csv")
  {
    writer_type = "csv";
  }
  else
  {
    writer_type = "json";
  }

  auto config = readConfig("gui_metadata_writer.conf", d->prefix);
  config->set_value("metadata_writer:type", writer_type,
                    "Type of serialization to use");
  d->freestandingConfig->merge_config(config);

  try
  {
    kv::algo::metadata_map_io_sptr metadataMapIO;
    kv::algo::metadata_map_io::set_nested_algo_configuration(
      "metadata_writer", config, metadataMapIO);
    if (!metadataMapIO)
    {
      QMessageBox::critical(
        this, QStringLiteral("Error saving metadata"),
        QStringLiteral("Improper configuration settings. "
                       "Please see the log."));
      return;
    }

    metadataMapIO->save(stdString(path), d->videoMetadataMap);
  }
  catch (std::exception const& e)
  {
    QMessageBox::critical(
      this, QStringLiteral("Error saving metadata"),
      QStringLiteral("Failed to save metadata: ") +
      QString::fromLocal8Bit(e.what()));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::saveKLV()
{
  QTE_D();

  auto const name = QFileInfo(d->videoPath).baseName();
  auto const path = QFileDialog::getSaveFileName(
    this, "Save KLV", name + QString(".json"),
    "JavaScript Object Notation (*.json);;"
    "All Files (*)");

  try
  {
    if (!path.isEmpty())
    {
      this->saveKLVFile(path);
    }
  }
  catch (...)
  {
    static auto const msg =
      QStringLiteral("An error occurred while saving the KLV to \"%1\". "
                     "The output file may not have been written correctly.");
    QMessageBox::critical(this, "Export error", msg.arg(path));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::saveKLVFile(QString const& path)
{
  QTE_D();

  typedef kwiversys::SystemTools ST;

  auto config = readConfig("gui_klv_writer.conf", d->prefix);
  try
  {
    kv::algo::metadata_map_io_sptr io;
    kv::algo::metadata_map_io::set_nested_algo_configuration(
      "metadata_writer", config, io);
    if (!io)
    {
      QMessageBox::critical(
        this, QStringLiteral("Error saving KLV"),
        QStringLiteral("Improper configuration settings. "
                       "Please see the log."));
      return;
    }

    io->save(stdString(path), d->videoMetadataMap);
  }
  catch (std::exception const& e)
  {
    QMessageBox::critical(
      this, QStringLiteral("Error saving KLV"),
      QStringLiteral("Failed to save KLV: ") +
      QString::fromLocal8Bit(e.what()));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::removeBurnIn()
{
  QTE_D();

  if (!d->videoSource)
  {
    return;
  }

  auto const& pipePath =
    findConfig(QStringLiteral("default.pipe"), d->prefix);
  if (pipePath.isEmpty())
  {
    QMessageBox::information(
      this, QStringLiteral("Not found"),
      QStringLiteral("The pipeline configuration could not be located. "
                     "Please check your installation."));
    return;
  }

  BurnoutDialog bod{this};
  bod.setDefaultPath(d->videoPath);
  if (bod.exec() != QDialog::Accepted)
  {
    return;
  }

  PipelineWindow w{this};

  w.setVideo(d->freestandingConfig, stdString(d->videoPath));
  if (w.setPipeline(pipePath))
  {
    auto const& vp = bod.videoPath();
    if (!vp.isEmpty())
    {
      auto encoder = krest::make_unique<VideoEncoder>();
      if (encoder->start(vp, d->videoSource->implementation_settings()))
      {
        w.attachEncoder(std::move(encoder), ImageSource::InpaintedImage);
      }
    }

    auto const& mp = bod.maskPath();
    if (!mp.isEmpty())
    {
      auto encoder = krest::make_unique<VideoEncoder>();
      if (encoder->start(mp, d->videoSource->implementation_settings()))
      {
        w.attachEncoder(std::move(encoder), ImageSource::MaskImage);
      }
    }

    w.exec();
  }
  else
  {
    qWarning() << "set pipeline failed";
    // TODO report error
  }
}

//-----------------------------------------------------------------------------
void MainWindow::setSlideSpeed(int speed)
{
  QTE_D();

  static auto const ttFormat =
    QString("%1 (%2)").arg(d->UI.slideSpeed->toolTip());

  auto dt = QString("Unconstrained");
  auto delay = 0.0;
  if (speed < 60)
  {
    auto const de = static_cast<double>(speed) * 0.1;
    auto const fps = qPow(2.0, de);
    delay = 1e3 / fps;
    dt = QString("%1 / sec").arg(fps, 0, 'g', 2);
  }
  d->slideTimer.setInterval(static_cast<int>(delay));
  d->UI.slideSpeed->setToolTip(ttFormat.arg(dt));
}

//-----------------------------------------------------------------------------
void MainWindow::setSlideshowPlaying(bool playing)
{
  QTE_D();
  if (playing)
  {
    if (d->UI.frame->value() == d->UI.frame->maximum())
    {
      d->UI.frame->triggerAction(QAbstractSlider::SliderToMinimum);
    }
    d->slideTimer.start();
  }
  else
  {
    d->slideTimer.stop();
  }

  d->UI.frame->setEnabled(!playing);
}

//-----------------------------------------------------------------------------
void MainWindow::nextSlide()
{
  QTE_D();

  if (d->UI.frame->value() == d->UI.frame->maximum())
  {
    if (d->UI.actionSlideshowLoop->isChecked())
    {
      d->UI.frame->triggerAction(QAbstractSlider::SliderToMinimum);
    }
    else
    {
      d->UI.actionSlideshowPlay->setChecked(false);
    }
  }
  else
  {
    d->UI.frame->triggerAction(QAbstractSlider::SliderSingleStepAdd);
  }
}

//-----------------------------------------------------------------------------
void MainWindow::setActiveFrame(int id)
{
  QTE_D();

  if (id < 1 || id > d->frameCount)
  {
    qDebug() << "MainWindow::setActiveFrame:"
             << " requested ID" << id << "is invalid";
    return;
  }

  d->setActiveFrame(id);
}

//-----------------------------------------------------------------------------
void MainWindow::updateFrames(
  std::shared_ptr<kv::metadata_map::map_metadata_t> mdMap)
{
  QTE_D();

  d->updateFrames(mdMap);
}

//-----------------------------------------------------------------------------
void MainWindow::setViewBackroundColor()
{
  QTE_D();

  QColorDialog dlg;
  dlg.setCurrentColor(*d->viewBackgroundColor);
  if (dlg.exec() == QDialog::Accepted)
  {
    *d->viewBackgroundColor = dlg.currentColor();
    // TODO not supported at this time
  }
}

//-----------------------------------------------------------------------------
void MainWindow::showAboutDialog()
{
  AboutDialog dlg(this);
  dlg.exec();
}

//-----------------------------------------------------------------------------
void MainWindow::showUserManual()
{
  QTE_D();

  auto const path = findUserManual(d->prefix);
  if (!path.isEmpty())
  {
    auto const& uri = QUrl::fromLocalFile(path);
    QDesktopServices::openUrl(uri);
  }
  else
  {
    QMessageBox::information(
      this, QStringLiteral("Not found"),
      QStringLiteral("The user manual could not be located. "
                     "Please check your installation."));
  }
}

//-----------------------------------------------------------------------------
void MainWindow::updateProgress(QString const& desc, int progress)
{
  QTE_D();

  d->updateProgress(this->sender(), desc, progress);
}

//END MainWindow
