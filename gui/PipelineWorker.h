/*ckwg +29
 * Copyright 2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BURNOUT_PIPELINEWORKER_H_
#define BURNOUT_PIPELINEWORKER_H_

#include <ImageContainer.h>

#include <arrows/qt/EmbeddedPipelineWorker.h>

#include <vital/config/config_block.h>

#include <qtGlobal.h>

namespace krest
{

namespace core
{

class VideoSource;

} // namespace core

} // namespace krest

class PipelineWorkerPrivate;

// ----------------------------------------------------------------------------
class PipelineWorker
  : public kwiver::arrows::qt::EmbeddedPipelineWorker
{
  Q_OBJECT

public:
  explicit PipelineWorker(QWidget* parent = nullptr);
  explicit PipelineWorker(RequiredEndcaps, QWidget* parent = nullptr);

  ~PipelineWorker() override;

  void setVideo(kwiver::vital::config_block_sptr const& config,
                std::string const& path);

  bool initialize(QString const& pipelineFile) override;
  void cancel();

signals:
  void inpaintedImageAvailable(kwiver::vital::image_container_sptr image);
  void maskImageAvailable(kwiver::vital::image_container_sptr mask);

  void progressRangeChanged(int minimum, int maximum);
  void progressValueChanged(int value);

  void endOfInput();

protected:
  QTE_DECLARE_PRIVATE_RPTR(PipelineWorker);

  void initializeInput(kwiver::embedded_pipeline& pipeline) override;

  void sendInput(kwiver::embedded_pipeline& pipeline) override;

  void processOutput(
    kwiver::adapter::adapter_data_set_t const& output) override;

  void reportError(QString const& message, QString const& subject) override;

private:
  QTE_DECLARE_PRIVATE(PipelineWorker);
};

#endif
