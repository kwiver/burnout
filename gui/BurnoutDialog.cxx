/*ckwg +29
 * Copyright 2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "BurnoutDialog.h"

#include "ui_BurnoutDialog.h"

#include <qtUiState.h>

#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QPushButton>

//-----------------------------------------------------------------------------
class BurnoutDialogPrivate
{
public:
  void chooseOutputPath(QLineEdit* widget);
  bool validate(QLineEdit const* path, QStringList& existing, QWidget* q);

  static QString defaultSuffix();

  Ui::BurnoutDialog UI;
  qtUiState uiState;
};

QTE_IMPLEMENT_D_FUNC(BurnoutDialog)

//-----------------------------------------------------------------------------
QString BurnoutDialogPrivate::defaultSuffix()
{
  return QStringLiteral("ts");
}

//-----------------------------------------------------------------------------
void BurnoutDialogPrivate::chooseOutputPath(QLineEdit* widget)
{
  QFileDialog fd{widget->window(), QStringLiteral("Save Video")};
  fd.setSupportedSchemes({QStringLiteral("file")});
  fd.setAcceptMode(QFileDialog::AcceptSave);
  fd.setOption(QFileDialog::DontConfirmOverwrite, true);
  fd.setDefaultSuffix(this->defaultSuffix());
  fd.setNameFilters({
    QStringLiteral("Video Files (*.ts *.mp4 *.mov *.avi *.mpg *.mpeg)"),
    QStringLiteral("All Files (*)"),
  });

  if (fd.exec() == QDialog::Accepted)
  {
    widget->setText(fd.selectedFiles().first());
  }
}

//-----------------------------------------------------------------------------
bool BurnoutDialogPrivate::validate(
  QLineEdit const* pathWidget, QStringList& existing, QWidget* q)
{
  auto const& path = pathWidget->text();
  auto const fi = QFileInfo{path};

  if (fi.exists() && !fi.isDir())
  {
    // If the file already exists, it's valid
    existing.append(path);
    return true;
  }

  auto const& pd = fi.dir();
  if (!pd.exists())
  {
    static auto const msg =
      QStringLiteral("The requested output path \"%1\" is not valid. "
                     "The path refers to a directory which does not exist.");
    QMessageBox::critical(q, QStringLiteral("Invalid path"), msg.arg(path));
    return false;
  }

  return true;
}

//-----------------------------------------------------------------------------
BurnoutDialog::BurnoutDialog(QWidget* parent, Qt::WindowFlags flags)
  : QDialog(parent, flags), d_ptr(new BurnoutDialogPrivate)
{
  QTE_D();

  // Set up UI
  d->UI.setupUi(this);

  // Set up option persistence
  d->uiState.setCurrentGroup("BurninRemoval");
  d->uiState.mapChecked("WriteVideo", d->UI.writeVideo);
  d->uiState.mapChecked("WriteMask", d->UI.writeMask);

  d->uiState.restore();

  // Connect signals/slots
  connect(d->UI.browseForVideo, &QAbstractButton::clicked, this,
          [d]{ d->chooseOutputPath(d->UI.videoPath); });
  connect(d->UI.browseForMask, &QAbstractButton::clicked, this,
          [d]{ d->chooseOutputPath(d->UI.maskPath); });
}

//-----------------------------------------------------------------------------
BurnoutDialog::~BurnoutDialog()
{
}

//-----------------------------------------------------------------------------
void BurnoutDialog::setDefaultPath(QString const& path)
{
  QTE_D();

  auto const fi = QFileInfo{path};
  auto const& basePath =
    fi.canonicalPath() + QDir::separator() + fi.completeBaseName();
  auto const& suffix = d->defaultSuffix();

  if (!d->UI.videoPath->isModified())
  {
    d->UI.videoPath->setText(basePath + "-inpainted." + suffix);
  }
  if (!d->UI.maskPath->isModified())
  {
    d->UI.maskPath->setText(basePath + "-mask." + suffix);
  }
}

//-----------------------------------------------------------------------------
void BurnoutDialog::accept()
{
  QTE_D();

  auto const writeVideo = d->UI.writeVideo->isChecked();
  auto const writeMask = d->UI.writeMask->isChecked();
  auto existing = QStringList{};

  if ((!writeVideo || d->validate(d->UI.videoPath, existing, this)) &&
      (!writeMask || d->validate(d->UI.maskPath, existing, this)))
  {
    if (!existing.isEmpty())
    {
      auto msg =
        QStringLiteral("<p>The following file(s) already exist. "
                       "Do you wish to overwrite?</p><ul>");
      for (auto const& p : existing)
      {
        msg += QStringLiteral("<li><code>%1</code></li>").arg(p);
      }
      msg += QStringLiteral("</ul>");

      QMessageBox mb{QMessageBox::Warning,
                     QStringLiteral("Overwrite File(s)?"),
                     msg, QMessageBox::Cancel, this};

      auto* const ob = mb.addButton(QStringLiteral("Overwrite"),
                                    QMessageBox::DestructiveRole);
      ob->setIcon(QIcon::fromTheme(QStringLiteral("document-replace")));

      mb.setDefaultButton(QMessageBox::Cancel);

      mb.exec();
      if (mb.clickedButton() != ob)
      {
        return;
      }
    }

    d->uiState.save();
    QDialog::accept();
  }
}

//-----------------------------------------------------------------------------
QString BurnoutDialog::videoPath() const
{
  QTE_D();

  if (d->UI.writeVideo->isChecked())
  {
    return d->UI.videoPath->text();
  }

  return {};
}

//-----------------------------------------------------------------------------
QString BurnoutDialog::maskPath() const
{
  QTE_D();

  if (d->UI.writeMask->isChecked())
  {
    return d->UI.maskPath->text();
  }

  return {};
}
