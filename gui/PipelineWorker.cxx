/*ckwg +29
 * Copyright 2021 by Kitware, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name Kitware, Inc. nor the names of any contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "PipelineWorker.h"

#include <krest/core/KwiverPipelinePortSet.hpp>

#include <krest/util/unique.hpp>

#include <arrows/qt/image_container.h>

#include <vital/algo/video_input.h>

#include <QApplication>
#include <QMessageBox>
#include <QThread>

#include <atomic>

namespace ka = kwiver::adapter;
namespace kv = kwiver::vital;
namespace kva = kwiver::vital::algo;

using super = kwiver::arrows::qt::EmbeddedPipelineWorker;

namespace
{

static char const* const VIDEOREADER_BLOCK = "video_reader";
static char const* const INPUT_IMAGE_PORT = "image";
static char const* const MASK_IMAGE_PORT = "mask";
static char const* const INPAINTED_IMAGE_PORT = "inpainted";

}

// ============================================================================
class PipelineWorkerPrivate
{
public:
  kva::video_input_sptr videoReader;
  kv::config_block_sptr config = kv::config_block::empty_config();
  std::string videoPath;

  int frameCount = 0;
  std::atomic<bool> canceled;
};

QTE_IMPLEMENT_D_FUNC(PipelineWorker)

// ----------------------------------------------------------------------------
PipelineWorker::PipelineWorker(QWidget* parent)
  : PipelineWorker{RequiresInput, parent}
{
}

// ----------------------------------------------------------------------------
PipelineWorker::PipelineWorker(
  RequiredEndcaps endcaps, QWidget* parent)
  : super{endcaps, parent}, d_ptr{new PipelineWorkerPrivate}
{
}

// ----------------------------------------------------------------------------
PipelineWorker::~PipelineWorker()
{
}

// ----------------------------------------------------------------------------
void PipelineWorker::setVideo(
  kv::config_block_sptr const& config, std::string const& path)
{
  QTE_D();

  d->config->merge_config(config);
  d->videoPath = path;
}

// ----------------------------------------------------------------------------
void PipelineWorker::cancel()
{
  QTE_D();
  d->canceled = true;
}

// ----------------------------------------------------------------------------
bool PipelineWorker::initialize(QString const& pipelineFile)
{
  QTE_D();

  try
  {
    if (!kva::video_input::check_nested_algo_configuration(
      VIDEOREADER_BLOCK, d->config))
    {
      this->reportError(
        QStringLiteral("An error was found in the video source"
                       " algorithm configuration"),
        QStringLiteral("Pipeline Error"));
      return false;
    }

    kva::video_input::set_nested_algo_configuration(
      VIDEOREADER_BLOCK, d->config, d->videoReader);

    d->videoReader->open(d->videoPath);
    d->frameCount = static_cast<int>(d->videoReader->num_frames());
  }
  catch (kv::vital_exception const& e)
  {
    this->reportError(
      QStringLiteral("Failed to load video: ") +
      QString::fromLocal8Bit(e.what()),
      QStringLiteral("Pipeline Error"));
    return false;
  }

  return super::initialize(pipelineFile);
}

// ----------------------------------------------------------------------------
void PipelineWorker::initializeInput(kwiver::embedded_pipeline& pipeline)
{
  QTE_D();

  emit this->progressRangeChanged(0, d->frameCount);

  super::initializeInput(pipeline);
}

// ----------------------------------------------------------------------------
void PipelineWorker::sendInput(kwiver::embedded_pipeline& pipeline)
{
  QTE_D();

  auto framesProcessed = int{0};

  d->canceled = false;
  while (!d->canceled)
  {
    kv::timestamp ts;
    if (!d->videoReader->next_frame(ts))
    {
      if (!d->videoReader->end_of_video())
      {
        this->reportError(
          QStringLiteral("Failed to read image from video"),
          QStringLiteral("Pipeline Error"));
      }
      break;
    }

    auto inputDataSet = ka::adapter_data_set::create();
    inputDataSet->add_value(INPUT_IMAGE_PORT, d->videoReader->frame_image());
    pipeline.send(inputDataSet);

    emit this->progressValueChanged(++framesProcessed);
  }

  pipeline.send_end_of_input();
  emit this->endOfInput();
}

// ----------------------------------------------------------------------------
void PipelineWorker::processOutput(ka::adapter_data_set_t const& output)
{
  auto get = [&](char const* port){
    return output->value<kv::image_container_sptr>(port);
  };

  if (auto const& image = get(INPAINTED_IMAGE_PORT))
  {
    emit this->inpaintedImageAvailable(image);
  }
  if (auto const& image = get(MASK_IMAGE_PORT))
  {
    emit this->maskImageAvailable(image);
  }
}

// ----------------------------------------------------------------------------
void PipelineWorker::reportError(
  QString const& message, QString const& subject)
{
  QTE_THREAD_INVOKE(this->reportError, message, subject);

  auto* const p = qobject_cast<QWidget*>(this->parent());
  auto* const w = (p ? p : qApp->activeWindow());
  QMessageBox::warning(w, subject, message);
}
