===============================================================================
  BurnOut
===============================================================================

.. role:: f
   :class: math

.. contents::

Overview
========

The BurnOut graphical application provides 

Camera View
===========

The camera view provides a camera space view of detected feature points and
computed landmarks and ground control points (both projected to the camera
space), as well as the corresponding input imagery, for the active camera.
Additionally, the estimation residuals |--| the difference between landmarks
and feature points which participated in computing their estimated positions
|--| can be displayed as line segments between the feature point location and
projected landmark location.

Tool Bar
--------

:icon:`view-reset` Reset View
  Resets the view to the camera image extents. Additional actions are available
  via the action's associated pop-up.

:icon:`blank` Zoom Extents
  Resets the view extents so that the entire scene is visible. This action is
  available via the :action:`view-reset Reset View` button's associated pop-up
  menu.

Frame Selection
================

The frame selection panel contains a large slider used to select the active
frame. A spin box next to the slider shows the active frame number, and can
also be used to set the active frame to seek in the video. Note that the frame
numbers need not be consecutive.  Some video readers are configured to only
read every `N`-th frame, where `N` may be 10, for example.  This help cut down
on data redundancy in video.  The frame sampling rate can be configured by
opening the project configuration file (``.conf``) in a text editor.

The controls to the right of the panel control the application's playback rate.
The play button automatically plays through the frames at fixed rate.
The small slider controls the delay between slides. The slider response
is logarithmic, with single steps in one-tenth powers of ten. The slider tool
tip includes the current delay in human readable units.

Metadata
========

The metadata panel displays the collection of video metadata for the current
frame, if available. The set of fields is selected from the entire data set;
individual frames may be missing some or all fields. The metadata itself is
provied by the video reader.  For encoded video files, BurnOut supports
key-length-value (KLV) encoding following the motion imagery standards board
(MISB) 0104 and 0601 standards.  Customized video readers can read metadata
from other sources, just as supplimentary text files or EXIF data.

Menu
====

File Menu
---------

:icon:`blank` New Project
  Select a working directory for a project. A project directory must be set
  before the tools in the Compute menu can be run. These tool will write files
  into the project working directory. A configuration file with the same name
  as the directory is also created in the directory. The project configuration
  file stores references to the project data such as the source video and
  computed results that will be loaded back in when a project is opened.

:icon:`open` Open Project
  Select an existing project configuration. The project configuration will
  often include references to various data files which are frequently stored in
  the same directory as the project configuration.

:icon:`blank` Import
  Provides options for importing/loading various types of data into the current
  project. The user must select the type of data to be loaded, as some data
  files use the same file extension.

:icon:`blank` Export
  Provides options for exporting various data.

:icon:`quit` Quit
  Exits the application.

View Menu
---------

:icon:`playback-play` Play Video
  Toggles playback of the video.

:icon:`playback-loop` Loop Video
  Toggles if the video should restart from the beginning after the last
  frame. When disabled, the slideshow ends when the last frame becomes
  active.

:icon:`blank` Background Color
  Changes the background color of the world and camera views.

:icon:`blank` Antialias Views
  Toggles use of an anti-aliasing filter.
  Anti-aliasing is accomplished via a post-processing filter (FXAA) that may
  produce undesirable artifacts. At this time, anti-aliasing via multi-sampling
  (MSAA) is not supported.

Help Menu
---------

:icon:`help-manual` BurnOut User Manual
  Displays the user manual (i.e. this document) in the default web browser.

:icon:`burnout` About BurnOut
  Shows copyright and version information about the application.

.. |->|  unicode:: U+02192 .. rightwards arrow
.. |--|  unicode:: U+02014 .. em dash
.. |deg| unicode:: U+000B0 .. degree sign
   :ltrim:
