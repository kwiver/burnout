Burnout v0.13.0 Release Notes
=============================

This is a minor release of BurnOut which can now export all ST0601 and ST1108
KLV tags directly to JSON.

Updates since v0.12.0
---------------------

  * All MISB ST0601 tags can now be parsed.

  * The video reader now processes multiple KLV streams separately instead of
    combining them.
  
  * A new export option has been added which exports all KLV packets to JSON.

  * Additional metadata fields are derived from the source video encoding.

  * CSV export now exports N rows per frame, if there are N KLV streams.
