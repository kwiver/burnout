BurnOut v0.10.0 Release Notes
=================================

This is a redesign of the Burn-Out GUI that was previously part of ViViA.
The new GUI combines KLV metadata processing and burned-in metadata removal
into a single standalone application.

