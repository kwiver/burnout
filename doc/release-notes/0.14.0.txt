Burnout v0.14.0 Release Notes
=============================

This is a minor release of Burnout which adds a few new features and fixes
several bugs.

Updates since v0.13.1
---------------------

  * Fixed various seeking- and timestamp-related bugs caused by integer
    overflow.

  * Added a cancel button to the File menu.

  * Improved playback and seeking performance.

  * Added support for CUDA video acceleration.
