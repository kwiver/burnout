Burnout v0.16.3 Release Notes
=============================

Updates since v0.16.2
---------------------

* Fixed crash when running the burn-in removal tool.

* Cleaned up dump-klv default output.

* Improved recognition of sync vs async KLV streams.

* Fixed bugs when using FFmpeg's CUDA codecs.

* Fixed retention of MISP timestamps with FFmpeg 5.1.2.

* Added the ffmpeg_rewire video input.

* Disabled insertion of the SDT (PID 17) into MPEG-TS streams.

* Added support for obscure IMAP values.

* Added the new ST0601 Tag 143: Metadata Substream ID Pack.
