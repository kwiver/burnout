.. image:: doc/images/BurnOut_Logo.png
   :width: 768px
   :alt: BurnOut

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BurnOut Overview
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BurnOut is a cross-platform desktop application for analyzing metadata
in video files.  In particular BurnOut can read both KLV metadata streams
and parse metadata "burned" into the pixels of the video.  BurnOut can
extract the metadata to various file formats and also mask out or in-paint
the burned in metadata in the video to remove it.

.. image:: /doc/images/screenshot.png
   :width: 100%
   :alt: Windows Screenshot

More advanced users who wish to build the project from source should proceed to the
`Building BurnOut`_ section.

Installation
============
If you have downloaded an installer from the
`latest release`_
you can simply install BurnOut according to the instructions for your
operating system described below. If you are building BurnOut from source
you should proceed to `Building BurnOut`_ to create the installer before
completing the installation.

**Windows:** run the installer executable (exe) and follow the prompts in the
installer dialog. Administrative permission is required.

**Mac:** open the disk image (dmg), accept the license terms, then drag the
BurnOut application into the Applications folder.

**Linux:** open a bash/cmd shell and run the self extracting installer script
(sh). You can view additional installation options using
``./BurnOut-<version>-Linux-x86_64.sh --help``

The remainder of this document is aimed at developers who wish to build the
project from source.

Building BurnOut
=====================

BurnOut requires C++11 compliant compiler
(e.g. GCC 4.8.1, Clang 3.3, Visual Studio 2015).
BurnOut uses CMake (www.cmake.org) for easy cross-platform compilation. The
minimum required version of CMake is 3.9.5, but newer versions are recommended.

Building
--------
The build is directed by CMake to ensure it can be built on various platforms.
The code is built by a CMake 'superbuild', meaning as part of the build,
CMake will download and build any dependent libraries needed by BurnOut.
The build is also out of source, meaning the code base is to be separate from
the build files.  This means you will need two folders, one for the source code
and one for the build files.
Here is the quickest way to build via a cmd/bash shell.

Before building on Linux systems you must install the following packages:

.. code-block :: bash

  sudo apt-get install build-essential libgl1-mesa-dev libxt-dev
  sudo apt-get install libexpat1-dev libgtk2.0-dev

On Linux, to optionally build with Python and help menu documentation you will
also need to install the following:

.. code-block :: bash

  sudo apt-get install python3-dev python3-docutils

Set up the folder structure and obtain the source files. This can be done with
git or by downloading the files and extracting them. Then setup the folder(s)
to build the binary files.

.. code-block :: bash

  mkdir burnout
  cd burnout

  ## Place the code in a directory called src
  # Using git, clone into a new directory called src
  git clone https://github.com/Kitware/BurnOut.git src
  # Or unzip into a new directory called src
  unzip <file name>.zip src

  ## Create the folder where we will build the binaries
  mkdir builds
  cd builds
  # Instead of just one builds folder you can to make subfolders here for
  # different builds, for example: builds/debug and builds/release.
  # Each folder would then be built following the steps below but with different
  # configuration options

Generate the makefile/msvc solution to perform the superbuild using cmake.
A description of the configuration options can be found in `CMake Options`_.

.. code-block :: bash

  # From the build directory provide cmake the path to the source directory,
  # which can be relative or absolute.
  # Specify configurable options by prefacing them with the -D flag
  cmake -DCMAKE_BUILD_TYPE:STRING=Release ../src
  # Alternatively, you can use the 'ccmake' command line tool allows for
  # interactively selecting CMake options. This can be installed with
  # 'sudo apt-get install cmake-curses-gui'
  ccmake ../src
  # As a final option, you can use the the CMake GUI you can set the source and
  # build directories accordingly and then press the "Configure" and “Generate”
  # buttons

Build the installer target/project

.. code-block :: bash

  # On Linux/OSX/MinGW
  make
  # Once the Superbuild is complete, the burnout makefile will be placed in
  # the build/external/burnout-build directory

  # For MSVC
  # Open the BurnOut-Superbuild.sln, choose your build configuration,
  # from the 'Build' menu choose 'Build Solution'
  # When the build is complete you may close this solution.
  # To edit BurnOut code, open the
  # build/external/burnout-build/BurnOut.sln

You have now built a BurnOut installer similar to what is provided in the
`latest release`_ section. To install BurnOut on you machine, follow the
instructions above in `Installation`_.

CMake Options
-------------

============================= ===================================================
``CMAKE_BUILD_TYPE``          The compiler mode, usually ``Debug`` or ``Release``
``BURNOUT_ENABLE_PYTHON``     Enable Python bindings in KWIVER
``BURNOUT_ENABLE_MANUALS``    Turn on building the user documentation
``BURNOUT_ENABLE_TESTING``    Build the unit tests
``BURNOUT_SUPERBUILD``        Build as a superbuild (build Fletch and KWIVER)
============================= ===================================================

Mulit-Configuration Build Tools
'''''''''''''''''''''''''''''''

By default the CMAKE_BUILD_TYPE is set to Release.

Separate directories are required for Debug and Release builds, requiring CMake
to be run for each.

Even if you are using a Multi-Configuration build tool (like MSVC) to build
Debug you must select the Debug CMAKE_BUILD_TYPE. (On Windows in order to debug
a project all dependent projects must be build with Debug information.)

For MSVC users wanting a RelWithDebInfo build we recommend you still choose
Release for the superbuild.  Release and RelWithDebInfo are compatible with each
other, and Fletch will build its base libraries as Release.  MSVC solutions will
provide both Release and RelWithDebInfo configuration options. You will need to
open the ``<build/directory>/external/kwiver-build/KWIVER.sln`` and build this
solution with the RelWithDebInfo configuration.


Documentation
'''''''''''''

If ``BURNOUT_ENABLE_MANUALS`` is enabled, and CMake finds all dependencies,
then the user manuals are built as part of the normal build process under the target
"manuals".  The GUI manual can be viewed from inside the GUI by choosing the
"BurnOut User Manual" action from the "Help" menu.

To build the user manual(s), you need:

* Python
    version 3.4 or greater
    http://www.python.org/

* Docutils
    version 0.11 or greater
    http://docutils.sourceforge.net/

(At present, only the GUI has a user manual.  Other manuals may be added in the
future.)

Testing
'''''''

`Travis CI`_ is used for continued integration testing.
Travis CI is limited to a single platform (Ubuntu Linux), but provides
automated testing of all topic branches and pull requests whenever they are
created.

============================= =============
Travis CI **master** branch:  |CI:master|_
Travis CI **release** branch: |CI:release|_
============================= =============

Advanced Build
--------------

BurnOut is built on top of the KWIVER_ toolkit, which is in turn built on
the Fletch_ super build system.  As mentioned above, to make it easier to build
BurnOut, a "super-build" is provided to build both KWIVER and Fletch.
But, if you wish, you may point the BurnOut build to use your own KWIVER
builds.

If you would like BurnOut to use a prebuilt version of KWIVER, specify the
kwiver_DIR flag to CMake.  The kwiver_DIR is the KWIVER build directory root,
which contains the kwiver-config.cmake file.

.. code-block :: bash

    $ cmake ../../src -DCMAKE_BUILD_TYPE=Release -Dkwiver_DIR:PATH=<path/to/kwiver/build/dir>

You must ensure that the specified build of KWIVER was built with at least the following options set:

The required KWIVER flags can be found in this file : `<CMake/burnout-external-kwiver.cmake>`_

The required Fletch flags can be found in this file : `<CMake/burnout-external-fletch.cmake>`_

Overview of Directories
=======================

======================= ========================================================
``CMake``               contains CMake helper scripts
``config``              contains reusable default algorithm configuration files
``doc``                 contains release notes, manuals, and other documentation
``gui``                 contains the visualization GUI source code and headers
``gui/icons``           contains the visualization GUI icon resources
``packaging``           contains support files for CPack packaging
======================= ========================================================

Getting Help
============

BurnOut is a component of Kitware_'s collection of open source computer
vision tools and part of the KWIVER_ ecosystem. Please join the
`kwiver-users <http://public.kitware.com/mailman/listinfo/kwiver-users>`_
mailing list to discuss or to ask for help with using BurnOut.
For less frequent announcements about BurnOut and other KWIVER components,
please join the
`kwiver-announce <http://public.kitware.com/mailman/listinfo/kwiver-announce>`_
mailing list.


Acknowledgements
================

The authors would like to thank AFRL/Sensors Directorate for their support
of this work via SBIR Contract FA8650-14-C-1820. This document is approved for
public release via 88ABW-2015-2555.


.. Appendix I: References
.. ======================

.. _VIRAT Video Dataset: http://www.viratdata.org/
.. _Eigen: http://eigen.tuxfamily.org/
.. _Fletch: https://github.com/Kitware/fletch
.. _Kitware: http://www.kitware.com/
.. _KWIVER: http://www.kwiver.org/
.. _Qt: https://www.qt.io/
.. _Travis CI: https://travis-ci.org/
.. _latest release: https://github.com/Kitware/BurnOut/releases/latest

.. Appendix II: Text Substitutions
.. ===============================

.. |>=| unicode:: U+02265 .. greater or equal sign

.. |CI:master| image:: https://travis-ci.org/Kitware/BurnOut.svg?branch=master
.. |CI:release| image:: https://travis-ci.org/Kitware/BurnOut.svg?branch=release

.. _CI:master: https://travis-ci.org/Kitware/BurnOut
.. _CI:release: https://travis-ci.org/Kitware/BurnOut
