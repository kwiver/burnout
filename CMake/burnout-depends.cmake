###
# Find all package dependencies
#

message(STATUS "Looking for kwiver in : ${kwiver_DIR}")

find_package(krest REQUIRED)
find_package(kwiver REQUIRED)
if ( IS_DIRECTORY ${fletch_DIR} )
  find_package(fletch REQUIRED)
  message(STATUS "Using fletch from : ${fletch_DIR}")
endif()
include( kwiver-cmake-future )
include( kwiver-utils )
include( kwiver-flags )
include( kwiver-configcheck )
include( burnout-utils ) # local utilities

# We must find third party packages used by both KWIVER and BurnOut here
# otherwise CMake will be unable to find those targets.
# In the future, this should be exported by KWIVER.

set(Qt5_MODULES Core Widgets Xml)
find_package(Qt5 5.7 COMPONENTS ${Qt5_MODULES} REQUIRED)

find_package(qtExtensions REQUIRED)
include(${qtExtensions_USE_FILE})
if(QTE_QT_VERSION VERSION_EQUAL "4")
  message(FATAL_ERROR "${PROJECT_NAME} does not support Qt4. "
    "But QTE_QT_VERSION is ${QTE_QT_VERSION}. "
    "Please provide path to qtExtensions built with Qt version 5 or higher.")
endif()

find_package(Threads)
