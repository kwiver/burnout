#
# BurnOut CMake utilities entry point
#

# Pin the current directory as the root directory for BurnOut utility files
set(BURNOUT_UTIL_ROOT "${CMAKE_CURRENT_LIST_DIR}")

include("${CMAKE_CURRENT_LIST_DIR}/utils/burnout-utils-buildinfo.cmake")
