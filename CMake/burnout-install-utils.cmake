# Installation logic for BurnOut CMake utilities
#
# Variables that modify function:
#
#   burnout_cmake_install_dir
#     - Directory to install files to
#
set(utils_dir "${CMAKE_CURRENT_LIST_DIR}")

install(
  FILES "${utils_dir}/burnout-utils.cmake"
  DESTINATION "${burnout_cmake_install_dir}"
  )
install(
  DIRECTORY "${utils_dir}/utils"
            "${utils_dir}/tools"
  DESTINATION "${burnout_cmake_install_dir}"
  )
