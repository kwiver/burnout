# krest External Project

message(STATUS "Configuring external Krest")

list(APPEND BURNOUT_DEPENDENCIES krest)

ExternalProject_Add(krest
  DEPENDS ${KREST_DEPENDENCIES}
  PREFIX ${BURNOUT_BINARY_DIR}
  SOURCE_DIR ${BURNOUT_EXTERNAL_DIR}/krest
  BINARY_DIR ${BURNOUT_EXTERNAL_DIR}/krest-build
  STAMP_DIR ${BURNOUT_STAMP_DIR}
  GIT_REPOSITORY "https://github.com/kitware/krest.git"
  GIT_TAG c84ccc74dfa53f4b0a66715ae35f5441888d28d9
  #GIT_SHALLOW 1
  CMAKE_CACHE_ARGS
    -Dkwiver_DIR:PATH=${kwiver_DIR}
    -Dfletch_DIR:PATH=${fletch_DIR}
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DCMAKE_PREFIX_PATH:STRING=${CMAKE_PREFIX_PATH}
    -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
    -DCMAKE_BUILD_WITH_INSTALL_RPATH:BOOL=ON
    -DCMAKE_CONFIGURATION_TYPES:STRING=${CMAKE_CONFIGURATION_TYPES}
    -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
    -DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
    -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
    -DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS}
    ${CMAKE_CXX_COMPILER_LAUNCHER_FLAG}
    ${CMAKE_C_COMPILER_LAUNCHER_FLAG}
    -DCMAKE_EXE_LINKER_FLAGS:STRING=${CMAKE_EXE_LINKER_FLAGS}
    -DCMAKE_SHARED_LINKER_FLAGS:STRING=${CMAKE_SHARED_LINKER_FLAGS}
    -DMAKECOMMAND:STRING=${MAKECOMMAND}
    -DADDITIONAL_C_FLAGS:STRING=${ADDITIONAL_C_FLAGS}
    -DADDITIONAL_CXX_FLAGS:STRING=${ADDITIONAL_CXX_FLAGS}
  INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step."
)

set(KREST_DIR "${BURNOUT_EXTERNAL_DIR}/krest-build/${CMAKE_INSTALL_LIBDIR}/cmake/krest")
set(krest_DIR "${BURNOUT_EXTERNAL_DIR}/krest-build/${CMAKE_INSTALL_LIBDIR}/cmake/krest"
    CACHE PATH "Location of Krest" FORCE)
